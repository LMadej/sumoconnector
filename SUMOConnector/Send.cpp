#include "Storage.h"
#include "Socket.h"
#include "Send.h"
#include <iostream>
#define CMD_GET_TL_VARIABLE 0xa2
#define ID_LIST 0x00
#define TL_PHASE_DURATION 0x24
#define TYPE_STRINGLIST 0x0E
#define RTYPE_OK 0x00
#define TYPE_STRING 0x0C
#define CMD_SET_TL_VARIABLE 0xc2
#define TL_RED_YELLOW_GREEN_STATE 0x20
#define TYPE_INTEGER 0x09
#define CMD_GET_INDUCTIONLOOP_VARIABLE 0xa0
#define LAST_STEP_VEHICLE_NUMBER 0x10
#define CMD_GET_EDGE_VARIABLE 0xaa
#define TYPE_COMPOUND 0x0F
#define TL_COMPLETE_DEFINITION_RYG 0x2b
// result type: not implemented
#define RTYPE_NOTIMPLEMENTED 0x01
// result type: error
#define RTYPE_ERR 0xFF
TraCIAPI::TraCIAPI()
: trafficlights(*this), loop(*this), edge(*this), mySocket(0) {}
TraCIAPI::~TraCIAPI() {
}

void TraCIAPI::send_commandSimulationStep(SUMOTime time){
	Storage outMsg;
	// command length
	outMsg.writeUnsignedByte(1 + 1 + 4);
	// command id
	outMsg.writeUnsignedByte(CMD_SIMSTEP2);
	outMsg.writeInt(time);
	// send request message
	mySocket->sendExact(outMsg);
}
void TraCIAPI::send_commandClose() {
	Storage outMsg;
	// command length
	outMsg.writeUnsignedByte(1 + 1);
	// command id
	outMsg.writeUnsignedByte(CMD_CLOSE);
	mySocket->sendExact(outMsg);
}
void TraCIAPI::send_commandSetValue(int domID, int varID, const std::string& objID, Storage& content){
	Storage outMsg;
	// command length (domID, varID, objID, dataType, data)
	outMsg.writeUnsignedByte(1 + 1 + 1 + 4 + (int)objID.length() + (int)content.size());
	// command id
	outMsg.writeUnsignedByte(domID);
	// variable id
	outMsg.writeUnsignedByte(varID);
	// object id
	outMsg.writeString(objID);
	// data type
	outMsg.writeStorage(content);
	// send message
	mySocket->sendExact(outMsg);
}
void TraCIAPI::send_commandGetVariable(int domID, int varID, const std::string& objID, Storage* add) {
	Storage outMsg;
	// command length
	unsigned int length = 1 + 1 + 1 + 4 + (int)objID.length();
	if (add != 0) {
		length += (int)add->size();
	}
	outMsg.writeUnsignedByte(length);
	// command id
	outMsg.writeUnsignedByte(domID);
	// variable id
	outMsg.writeUnsignedByte(varID);
	// object id
	outMsg.writeString(objID);
	// additional values
	if (add != 0) {
		outMsg.writeStorage(*add);
	}
	// send request message
	mySocket->sendExact(outMsg);
}
void TraCIAPI::TrafficLightScope::setRedYellowGreenState(const std::string& tlsID, const std::string& state) const {
	Storage content;
	content.writeUnsignedByte(TYPE_STRING);
	content.writeString(state);
	myParent.send_commandSetValue(CMD_SET_TL_VARIABLE, TL_RED_YELLOW_GREEN_STATE, tlsID, content);
}
unsigned int TraCIAPI::InductionLoopScope::getLastStepVehicleNumber(const std::string& loopID) const {
	return myParent.getIntVeh(CMD_GET_INDUCTIONLOOP_VARIABLE, LAST_STEP_VEHICLE_NUMBER, loopID);
}

int TraCIAPI::getInt(int cmd, int var, const std::string& id, Storage* add) {
	Storage inMsg;
	send_commandGetVariable(cmd, var, id, add);
	processGET(inMsg, cmd, TYPE_INTEGER);
	return inMsg.readInt();
}
int TraCIAPI::getIntVeh(int cmd, int var, const std::string& id, Storage* add) {
	Storage inMsg;
	send_commandGetVariable(cmd, var, id, add);
	processGET(inMsg, cmd, TYPE_INTEGER);
	return inMsg.store[19];
}
unsigned int TraCIAPI::EdgeScope::getLastStepVehicleNumber(const std::string& edgeID) const {
	return myParent.getInt(CMD_GET_EDGE_VARIABLE, LAST_STEP_VEHICLE_NUMBER, edgeID);
}
void TraCIAPI::check_resultState(Storage& inMsg, int command, bool ignoreCommandId, std::string* acknowledgement) const {
	mySocket->receiveExact(inMsg);
	int cmdLength;
	int cmdId;
	int resultType;
	int cmdStart;
	std::string msg;
	try {
		cmdStart = inMsg.position();
		cmdLength = inMsg.readUnsignedByte();
		cmdId = inMsg.readUnsignedByte();
		if (command != cmdId && !ignoreCommandId) {
			std::cerr << "#Error: received status response to command: " << cmdId << " but expected: " << command;
		}
		resultType = inMsg.readUnsignedByte();
		msg = inMsg.readString();
	}
	catch (std::invalid_argument&) {
		std::cerr << "#Error: an exception was thrown while reading result state message";
	}
	switch (resultType) {
	case RTYPE_ERR:
		std::cerr << ".. Answered with error to command (" << command << "), [description: " << msg << "]";
	case RTYPE_NOTIMPLEMENTED:
		std::cerr << ".. Sent command is not implemented (" << command << "), [description: " << msg << "]";
	case RTYPE_OK:
		if (acknowledgement != 0) {
			//std::cerr << *acknowledgement<< ".. Command acknowledged ("<< command<< "), [description: "<< msg << "]";
		}
		break;
	default:
		break;
		std::cerr << ".. Answered with unknown result code(" << resultType << ") to command(" << command << "), [description: " << msg << "]";
	}
	if ((cmdStart + cmdLength) != (int)inMsg.position()) {
		std::cerr << "#Error: command at position " << cmdStart << " has wrong length" << (int)inMsg.position();
	}

}


void TraCIAPI::check_commandGetResult(Storage& inMsg, int command, int expectedType, bool ignoreCommandId) const {
	inMsg.position(); // respStart
	int length = inMsg.readUnsignedByte();
	if (length == 0) {
		length = inMsg.readInt();
	}
	int cmdId = inMsg.readUnsignedByte();
	if (!ignoreCommandId && cmdId != (command + 0x10)) {
		std::cerr << "#Error: received response with command id: " << cmdId << "but expected: " << (command + 0x10);
	}
	if (expectedType >= 0) {
		int valueDataType = inMsg.readUnsignedByte();
		if (valueDataType != expectedType) {
			//std::cerr << "Expected " <<expectedType<< " but got " <<valueDataType;
		}
	}
}
std::vector<TraCIAPI::TraCILogic> TraCIAPI::TrafficLightScope::getCompleteRedYellowGreenDefinition(const std::string& tlsID) const {
	Storage inMsg;
	myParent.send_commandGetVariable(CMD_GET_TL_VARIABLE, TL_COMPLETE_DEFINITION_RYG, tlsID);
	myParent.processGET(inMsg, CMD_GET_TL_VARIABLE, TYPE_COMPOUND);
	std::vector<TraCIAPI::TraCILogic> ret;
	int logicNo = inMsg.readInt();
	for (int i = 0; i < logicNo; ++i) {
		inMsg.readUnsignedByte();
		std::string subID = inMsg.readString();
		inMsg.readUnsignedByte();
		int type = inMsg.readInt();
		inMsg.readUnsignedByte();
		inMsg.readInt(); // add
		inMsg.readUnsignedByte();
		int phaseIndex = inMsg.readInt();
		inMsg.readUnsignedByte();
		int phaseNumber = inMsg.readInt();
		std::vector<TraCIAPI::TraCIPhase> phases;
		for (int j = 0; j < phaseNumber; ++j) {
			inMsg.readUnsignedByte();
			int duration = inMsg.readInt();
			inMsg.readUnsignedByte();
			int duration1 = inMsg.readInt();
			inMsg.readUnsignedByte();
			int duration2 = inMsg.readInt();
			inMsg.readUnsignedByte();
			std::string phase = inMsg.readString();
			phases.push_back(TraCIAPI::TraCIPhase(duration, duration1, duration2, phase));
		}
		ret.push_back(TraCIAPI::TraCILogic(subID, type, std::map<std::string, SUMOReal>(), phaseIndex, phases));
	}
	return ret;
}

void TraCIAPI::TrafficLightScope::setPhaseDuration(const std::string& tlsID, unsigned int phaseDuration) const {
	Storage content;
	content.writeUnsignedByte(TYPE_INTEGER);
	content.writeInt(int(1000 * phaseDuration));
	myParent.send_commandSetValue(CMD_SET_TL_VARIABLE, TL_PHASE_DURATION, tlsID, content);
}
void TraCIAPI::processGET(Storage& inMsg, int command, int expectedType, bool ignoreCommandId) const{
	check_resultState(inMsg, command, ignoreCommandId);
	check_commandGetResult(inMsg, command, expectedType, ignoreCommandId);
}
std::vector<std::string> TraCIAPI::getStringVector(int cmd, int var, const std::string& id, Storage* add) {
	Storage inMsg;
	send_commandGetVariable(cmd, var, id, add);
	processGET(inMsg, cmd, TYPE_STRINGLIST);
	unsigned int size = inMsg.readInt();
	std::vector<std::string> r;
	for (unsigned int i = 0; i < size; ++i) {
		r.push_back(inMsg.readString());
	}
	return r;
}
std::vector<std::string>TraCIAPI::TrafficLightScope::getIDList() const {
	return myParent.getStringVector(CMD_GET_TL_VARIABLE, ID_LIST, "");
}

void TraCIAPI::connect(std::string host, int port) {
	mySocket = new Socket(host, port);
	mySocket->connect();
}
