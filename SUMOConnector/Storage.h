#ifndef STORAGE_H
#define STORAGE_H
#include <vector>
#include <fstream>

class Storage{
public:
	typedef std::vector<unsigned char> StorageType;
	StorageType store;
private:
	
	StorageType::const_iterator iter_;
	void init();
	void checkReadSafe(unsigned int num) const throw(std::invalid_argument);
	bool bigEndian_;
	void writeByEndianess(const unsigned char * begin, unsigned int size);
	unsigned char readCharUnsafe();
	void readByEndianess(unsigned char * array, int size);
public:
	Storage();
	Storage(const unsigned char[], int length = -1);
	virtual bool valid_pos();
	void reset();
	virtual void writePacket(unsigned char* packet, int length);
	virtual void writePacket(const std::vector<unsigned char> &packet);
	virtual unsigned int position() const;
	virtual int readUnsignedByte() throw(std::invalid_argument);
	virtual void writeUnsignedByte(int) throw(std::invalid_argument);
	virtual int readInt() throw(std::invalid_argument);
	virtual void writeInt(int) ;
	virtual unsigned char readChar() throw(std::invalid_argument);
	virtual void writeChar(unsigned char) throw();
	virtual void writeStorage(Storage& store);
	void writeString(const std::string& s);
	 virtual std::string readString() throw(std::invalid_argument);
	StorageType::size_type size() const { return store.size(); }
	StorageType::const_iterator begin() const { return store.begin(); }
	StorageType::const_iterator end() const { return store.end(); }
};

#endif