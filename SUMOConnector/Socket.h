#ifndef SOCKET_H
#define SOCKET_H
#include <string>
#include <vector>
#include "Storage.h"


class Socket{
public:
	Socket(std::string host, int port);
	void connect();//+
	void send(const std::vector<unsigned char> &buffer);//+
	void sendExact(const Storage &);//+
	void close();//+
	bool receiveExact(Storage &);
protected:
	static const int lengthLen;
	void receiveComplete(unsigned char * const buffer, std::size_t len) const;
	size_t recvAndCheck(unsigned char * const buffer, std::size_t len) const;
private:
	void init();//+
	void BailOnSocketError(std::string);//+
#ifdef WIN32
	std::string GetWinsockErrorString(int err) const;
#endif
	bool atoaddr(std::string, struct in_addr& addr);
	bool datawaiting(int sock) const throw();
	std::string host_;
	int port_;
	int socket_;
	int server_socket_;
	bool blocking_;

	bool verbose_;
#ifdef WIN32
	static bool init_windows_sockets_;
	static bool windows_sockets_initialized_;
	static int instance_count_;
#endif
};

#endif