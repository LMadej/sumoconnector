#define _CRT_SECURE_NO_DEPRECATE
#define WINVER 0x0500
#include <windows.h>
#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "rapidxml.hpp"
#include "rapidxml_utils.hpp"
#include "rapidxml_iterators.hpp"
#include "rapidxml_print.hpp"
#include "Send.h"

using namespace rapidxml;
enum { _OK, _ERR, _RETURN, _BREAK, _CONTINUE };
enum { PT_ESC, PT_STR, PT_CMD, PT_VAR, PT_SEP, PT_EOL, PT_EOF };
using namespace std;
TraCIAPI A;
int stepcounter;
int z = -1;
string value;
struct Parser {
	char *text;
	char *p; /* current text position */
	int len; /* remaining length */
	char *start; /* token start */
	char *end; /* token end */
	int type; /* token type, PT_... */
	int insidequote; /* True if inside " " */
};

struct Var {
	char *name, *val;
	struct Var *next;
};

struct Interp; /* forward declaration */
typedef int(*CmdFunc)(struct Interp *i, int argc, char **argv, void *privdata);

struct Cmd {
	char *name;
	CmdFunc func;
	void *privdata;
	struct Cmd *next;
};

struct CallFrame {
	struct Var *vars;
	struct CallFrame *parent; /* parent is NULL at top level */
};

struct Interp {
	int level; /* Level of nesting */
	struct CallFrame *callframe;
	struct Cmd *commands;
	char *result;
};

void InitParser(struct Parser *p, char *text) {
	p->text = p->p = text;
	p->len = strlen(text);
	p->start = 0; p->end = 0; p->insidequote = 0;
	p->type = PT_EOL;
}

int ParseSep(struct Parser *p) {
	p->start = p->p;
	while (*p->p == ' ' || *p->p == '\t' || *p->p == '\n' || *p->p == '\r') {
		p->p++; p->len--;
	}
	p->end = p->p - 1;
	p->type = PT_SEP;
	return _OK;
}

int ParseEol(struct Parser *p) {
	p->start = p->p;
	while (*p->p == ' ' || *p->p == '\t' || *p->p == '\n' || *p->p == '\r' ||
		*p->p == ';')
	{
		p->p++; p->len--;
	}
	p->end = p->p - 1;
	p->type = PT_EOL;
	return _OK;
}

int ParseCommand(struct Parser *p) {
	int level = 1;
	int blevel = 0;
	p->start = ++p->p; p->len--;
	while (1) {
		if (p->len == 0) {
			break;
		}
		else if (*p->p == '[' && blevel == 0) {
			level++;
		}
		else if (*p->p == ']' && blevel == 0) {
			if (!--level) break;
		}
		else if (*p->p == '\\') {
			p->p++; p->len--;
		}
		else if (*p->p == '{') {
			blevel++;
		}
		else if (*p->p == '}') {
			if (blevel != 0) blevel--;
		}
		p->p++; p->len--;
	}
	p->end = p->p - 1;
	p->type = PT_CMD;
	if (*p->p == ']') {
		p->p++; p->len--;
	}
	return _OK;
}

int ParseVar(struct Parser *p) {
	p->start = ++p->p; p->len--; /* skip the $ */
	while (1) {
		if ((*p->p >= 'a' && *p->p <= 'z') || (*p->p >= 'A' && *p->p <= 'Z') ||
			(*p->p >= '0' && *p->p <= '9') || *p->p == '_')
		{
			p->p++; p->len--; continue;
		}
		break;
	}
	if (p->start == p->p) { /* It's just a single char string "$" */
		p->start = p->end = p->p - 1;
		p->type = PT_STR;
	}
	else {
		p->end = p->p - 1;
		p->type = PT_VAR;
	}
	return _OK;
}

int ParseBrace(struct Parser *p) {
	int level = 1;
	p->start = ++p->p; p->len--;
	while (1) {
		if (p->len >= 2 && *p->p == '\\') {
			p->p++; p->len--;
		}
		else if (p->len == 0 || *p->p == '}') {
			level--;
			if (level == 0 || p->len == 0) {
				p->end = p->p - 1;
				if (p->len) {
					p->p++; p->len--; /* Skip final closed brace */
				}
				p->type = PT_STR;
				return _OK;
			}
		}
		else if (*p->p == '{')
			level++;
		p->p++; p->len--;
	}
	return _OK; /* unreached */
}

int ParseString(struct Parser *p) {
	int newword = (p->type == PT_SEP || p->type == PT_EOL || p->type == PT_STR);
	if (newword && *p->p == '{') return ParseBrace(p);
	else if (newword && *p->p == '"') {
		p->insidequote = 1;
		p->p++; p->len--;
	}
	p->start = p->p;
	while (1) {
		if (p->len == 0) {
			p->end = p->p - 1;
			p->type = PT_ESC;
			return _OK;
		}
		switch (*p->p) {
		case '\\':
			if (p->len >= 2) {
				p->p++; p->len--;
			}
			break;
		case '$': case '[':
			p->end = p->p - 1;
			p->type = PT_ESC;
			return _OK;
		case ' ': case '\t': case '\n': case '\r': case ';':
			if (!p->insidequote) {
				p->end = p->p - 1;
				p->type = PT_ESC;
				return _OK;
			}
			break;
		case '"':
			if (p->insidequote) {
				p->end = p->p - 1;
				p->type = PT_ESC;
				p->p++; p->len--;
				p->insidequote = 0;
				return _OK;
			}
			break;
		}
		p->p++; p->len--;
	}
	return _OK; /* unreached */
}

int ParseComment(struct Parser *p) {
	while (p->len && *p->p != '\n') {
		p->p++; p->len--;
	}
	return _OK;
}

int GetToken(struct Parser *p) {
	while (1) {
		if (!p->len) {
			if (p->type != PT_EOL && p->type != PT_EOF)
				p->type = PT_EOL;
			else
				p->type = PT_EOF;
			return _OK;
		}
		switch (*p->p) {
		case ' ': case '\t': case '\r':
			if (p->insidequote) return ParseString(p);
			return ParseSep(p);
		case '\n': case ';':
			if (p->insidequote) return ParseString(p);
			return ParseEol(p);
		case '[':
			return ParseCommand(p);
		case '$':
			return ParseVar(p);
		case '#':
			if (p->type == PT_EOL) {
				ParseComment(p);
				continue;
			}
			return ParseString(p);
		default:
			return ParseString(p);
		}
	}
	return _OK; /* unreached */
}

void InitInterp(struct Interp *i) {
	i->level = 0;
	i->callframe = static_cast<CallFrame *>(malloc(sizeof(struct CallFrame)));
	i->callframe->vars = NULL;
	i->callframe->parent = NULL;
	i->commands = NULL;
	i->result = _strdup("");
}

void SetResult(struct Interp *i, char *s) {
	free(i->result);
	i->result = _strdup(s);
}

struct Var *GetVar(struct Interp *i, char *name) {
	struct Var *v = i->callframe->vars;
	while (v) {
		if (strcmp(v->name, name) == 0) return v;
		v = v->next;
	}
	return NULL;
}

int SetVar(struct Interp *i, char *name, char *val) {
	struct Var *v = GetVar(i, name);
	if (v) {
		free(v->val);
		v->val = _strdup(val);
	}
	else {
		v = static_cast<Var *>(malloc(sizeof(*v)));
		v->name = _strdup(name);
		v->val = _strdup(val);
		v->next = i->callframe->vars;
		i->callframe->vars = v;
	}
	return _OK;
}
struct Cmd *GetCommand(struct Interp *i, char *name) {
	struct Cmd *c = i->commands;
	while (c) {
		if (strcmp(c->name, name) == 0) return c;
		c = c->next;
	}
	return NULL;
}

int RegisterCommand(struct Interp *i, char *name, CmdFunc f, void *privdata) {
	struct Cmd *c = GetCommand(i, name);
	char errbuf[1024];
	if (c) {
		_snprintf_s(errbuf, 1024, "Command '%s' already defined", name);
		SetResult(i, errbuf);
		return _ERR;
	}
	c = static_cast<Cmd *>(malloc(sizeof(*c)));
	c->name = _strdup(name);
	c->func = f;
	c->privdata = privdata;
	c->next = i->commands;
	i->commands = c;
	return _OK;
}

/* EVAL! */
int Eval(struct Interp *i, char *t) {
	struct Parser p;
	int argc = 0, j;
	char **argv = NULL;
	char errbuf[1024];
	int retcode = _OK;
	SetResult(i, "");
	InitParser(&p, t);
	while (1) {
		char *t;
		int tlen;
		int prevtype = p.type;
		GetToken(&p);
		if (p.type == PT_EOF) break;
		tlen = p.end - p.start + 1;
		if (tlen < 0) tlen = 0;
		t = static_cast<char *>(malloc(tlen + 1));
		memcpy(t, p.start, tlen);
		t[tlen] = '\0';
		if (p.type == PT_VAR) {
			struct Var *v = GetVar(i, t);
			if (!v) {
				_snprintf_s(errbuf, 1024, "No such variable '%s'", t);
				free(t);
				SetResult(i, errbuf);
				retcode = _ERR;
				goto err;
			}
			free(t);
			t = _strdup(v->val);
		}
		else if (p.type == PT_CMD) {
			retcode = Eval(i, t);
			free(t);
			if (retcode != _OK) goto err;
			t = _strdup(i->result);
		}
		else if (p.type == PT_ESC) {
			/* XXX: escape handling missing! */
		}
		else if (p.type == PT_SEP) {
			prevtype = p.type;
			free(t);
			continue;
		}
		/* We have a complete command + args. Call it! */
		if (p.type == PT_EOL) {
			struct Cmd *c;
			free(t);
			prevtype = p.type;
			if (argc) {
				if ((c = GetCommand(i, argv[0])) == NULL) {
					_snprintf_s(errbuf, 1024, "No such command '%s'. Type 'help' to see which commands could be used.", argv[0]);
					SetResult(i, errbuf);
					retcode = _ERR;
					goto err;
				}
				retcode = c->func(i, argc, argv, c->privdata);
				if (retcode != _OK) goto err;
			}
			/* Prepare for the next command */
			for (j = 0; j < argc; j++) free(argv[j]);
			free(argv);
			argv = NULL;
			argc = 0;
			continue;
		}
		/* We have a new token, append to the previous or as new arg? */
		if (prevtype == PT_SEP || prevtype == PT_EOL) {
			argv = static_cast<char **>(realloc(argv, sizeof(char*)*(argc + 1)));
			argv[argc] = t;
			argc++;
		}
		else { /* Interpolation */
			int oldlen = strlen(argv[argc - 1]), tlen = strlen(t);
			argv[argc - 1] = static_cast<char *>(realloc(argv[argc - 1], oldlen + tlen + 1));
			memcpy(argv[argc - 1] + oldlen, t, tlen);
			argv[argc - 1][oldlen + tlen] = '\0';
			free(t);
		}
		prevtype = p.type;
	}
err:
	for (j = 0; j < argc; j++) free(argv[j]);
	free(argv);
	return retcode;
}

int ArityErr(struct Interp *i, char *name) {
	char buf[1024];
	_snprintf_s(buf, 1024, "Wrong number of args for %s", name);
	SetResult(i, buf);
	return _ERR;
}

int CommandMath(struct Interp *i, int argc, char **argv, void *pd) {
	char buf[64]; int a, b, c;
	if (argc != 3) return ArityErr(i, argv[0]);
	a = atoi(argv[1]); b = atoi(argv[2]);
	if (argv[0][0] == '+') c = a + b;
	else if (argv[0][0] == '-') c = a - b;
	else if (argv[0][0] == '*') c = a*b;
	else if (argv[0][0] == '/') c = a / b;
	else if (argv[0][0] == '%') c = (int)a % (int)b;
	else if (argv[0][0] == '>' && argv[0][1] == '\0') c = a > b;
	else if (argv[0][0] == '>' && argv[0][1] == '=') c = a >= b;
	else if (argv[0][0] == '<' && argv[0][1] == '\0') c = a < b;
	else if (argv[0][0] == '<' && argv[0][1] == '=') c = a <= b;
	else if (argv[0][0] == '=' && argv[0][1] == '=') c = a == b;
	else if (argv[0][0] == '!' && argv[0][1] == '=') c = a != b;
	else c = 0;
	_snprintf_s(buf, 64, "%d", c);
	SetResult(i, buf);
	return _OK;
}

int CommandSet(struct Interp *i, int argc, char **argv, void *pd) {
	if (argc != 3) return ArityErr(i, argv[0]);
	SetVar(i, argv[1], argv[2]);
	SetResult(i, argv[2]);
	return _OK;
}
int CommandGetTab(struct Interp *i, int argc, char **argv, void *pd) {
	struct Var *v = GetVar(i, argv[1]);
	char buf[100];
	value = v->val;
	string time;
	int a = value.length();
	switch (atoi(argv[3]))
	{
	case 0:
		time = value[0];
		time = time + value[1];
		break;
	case 1:
		time = value[2];
		break;
	case 2:
		time = value[3];
		time = time + value[4];
		break;
	case 3:
		time = value[5];
		break;
	}

	sprintf(buf, "%s", time.c_str());
	SetVar(i, argv[2], buf);
	SetResult(i, buf);
	return 0;
}
int CommandGetTab3(struct Interp *i, int argc, char **argv, void *pd) {
	struct Var *v = GetVar(i, argv[1]);
	char buf[100];
	value = v->val;
	string time;
	time = value[atoi(argv[3])+z];
	z++;
	time = time + value[atoi(argv[3])+z];
	sprintf(buf, "%s", time.c_str());
	SetVar(i, argv[2], buf);
	SetResult(i, buf);
	return 0;
}
int CommandGetTab2(struct Interp *i, int argc, char **argv, void *pd) {
	struct Var *v = GetVar(i, argv[1]);
	char buf[100];
	string prog;
	value = v->val;
	int a = value.length();
	int b = (a / 4)-1;
	int c = atoi(argv[3]);
	c = c * b + atoi(argv[3]);
	for (int z = 0; z < b; z++)
	{
		prog = prog + value[z+c];
	}
	sprintf(buf, "%s", prog.c_str());
	SetVar(i, argv[2], buf);
	SetResult(i, buf);
	return 0;
}
int CommandGetFix(struct Interp *i, int argc, char **argv, void *pd) {
	struct Var *v = GetVar(i, argv[1]);
	char buf[100];
	string prog;
	value = v->val;
	int a = value.length();
	int b = (a / 4) - 1;
	prog = value[b];
	sprintf(buf, "%s", prog.c_str());
	SetVar(i, argv[2], buf);
	SetResult(i, buf);
	return 0;
}
int CommandSetTab(struct Interp *i, int argc, char **argv, void *pd) {
	struct Var *v = GetVar(i, argv[1]);
	value = v->val;
	char buf[100];
	int b = atoi(argv[3]);
	string pom = argv[2];
	value[b] = pom[0];
	sprintf(buf, "%s", value.c_str());
	SetVar(i, argv[1], buf);
	SetResult(i, buf);
	return 0;
}
int CommandGet(struct Interp *i, int argc, char **argv, void *pd)
{
	int veh = A.loop.getLastStepVehicleNumber(argv[1]);
	char buf[5];
	_snprintf_s(buf, 5, "%d", veh);
	SetVar(i, argv[2], buf);
	SetResult(i, buf);
	return 0;
}
int CommandPhase(struct Interp *i, int argc, char **argv, void *pd)
{
	std::string acknowledgement;
	Storage inMsg;
	int a = atoi(argv[2]);
	A.trafficlights.setPhaseDuration(argv[1], a);
	A.check_resultState(inMsg, CMD_SET_TL_VARIABLE, false, &acknowledgement);
	return 0;
}
int CommandDef(struct Interp *i, int argc, char **argv, void *pd)
{
	std::cout << argv[1] << std::endl;
	A.trafficlights.getCompleteRedYellowGreenDefinition(argv[1]);
	return 0;
}
int CommandSetLights(struct Interp *i, int argc, char **argv, void *pd) {
	std::string acknowledgement;
	Storage inMsg;
	if (argc != 3) return ArityErr(i, argv[0]);
	SetVar(i, argv[1], argv[2]);
	SetResult(i, argv[2]);
	A.trafficlights.setRedYellowGreenState(argv[1], argv[2]);
	A.check_resultState(inMsg, CMD_SET_TL_VARIABLE, false, &acknowledgement);
	return _OK;
}

int CommandPuts(struct Interp *i, int argc, char **argv, void *pd) {
	if (argc != 2) return ArityErr(i, argv[0]);
	printf("%s\n", argv[1]);
	return _OK;
}

int CommandIf(struct Interp *i, int argc, char **argv, void *pd) {
	int retcode;
	if (argc != 3 && argc != 5) return ArityErr(i, argv[0]);
	if ((retcode = Eval(i, argv[1])) != _OK) return retcode;
	if (atoi(i->result)) return Eval(i, argv[2]);
	else if (argc == 5) return Eval(i, argv[4]);
	return _OK;
}

int CommandWhile(struct Interp *i, int argc, char **argv, void *pd) {
	if (argc != 3) return ArityErr(i, argv[0]);
	while (1) {
		int retcode = Eval(i, argv[1]);
		if (retcode != _OK) return retcode;
		if (atoi(i->result)) {
			if ((retcode = Eval(i, argv[2])) == _CONTINUE) continue;
			else if (retcode == _OK) continue;
			else if (retcode == _BREAK) return _OK;
			else return retcode;
		}
		else {
			return _OK;
		}
	}
}

int CommandRetCodes(struct Interp *i, int argc, char **argv, void *pd) {
	if (argc != 1) return ArityErr(i, argv[0]);
	if (strcmp(argv[0], "break") == 0) return _BREAK;
	else if (strcmp(argv[0], "continue") == 0) return _CONTINUE;
	return _OK;
}

void DropCallFrame(struct Interp *i) {
	struct CallFrame *cf = i->callframe;
	struct Var *v = cf->vars, *t;
	while (v) {
		t = v->next;
		free(v->name);
		free(v->val);
		free(v);
		v = t;
	}
	i->callframe = cf->parent;
	free(cf);
}

int CommandCallProc(struct Interp *i, int argc, char **argv, void *pd) {
	char **x = static_cast<char **>(pd), *alist = x[0], *body = x[1], *p = _strdup(alist), *tofree;
	struct CallFrame* cf = static_cast<CallFrame*>(malloc(sizeof(*cf)));
	int arity = 0, done = 0, errcode = _OK;
	char errbuf[1024];
	cf->vars = NULL;
	cf->parent = i->callframe;
	i->callframe = cf;
	tofree = p;
	while (1) {
		char *start = p;
		while (*p != ' ' && *p != '\0') p++;
		if (*p != '\0' && p == start) {
			p++; continue;
		}
		if (p == start) break;
		if (*p == '\0') done = 1; else *p = '\0';
		if (++arity > argc - 1) goto arityerr;
		SetVar(i, start, argv[arity]);
		p++;
		if (done) break;
	}
	free(tofree);
	if (arity != argc - 1) goto arityerr;
	errcode = Eval(i, body);
	if (errcode == _RETURN) errcode = _OK;
	DropCallFrame(i); /* remove the called proc callframe */
	return errcode;
arityerr:
	_snprintf_s(errbuf, 1024, "Proc '%s' called with wrong arg num", argv[0]);
	SetResult(i, errbuf);
	DropCallFrame(i); /* remove the called proc callframe */
	return _ERR;
}

int CommandProc(struct Interp *i, int argc, char **argv, void *pd) {
	char **procdata = static_cast<char **>(malloc(sizeof(char*)* 2));
	if (argc != 4) return ArityErr(i, argv[0]);
	procdata[0] = _strdup(argv[2]); /* arguments list */
	procdata[1] = _strdup(argv[3]); /* procedure body */
	return RegisterCommand(i, argv[1], CommandCallProc, procdata);
}

int CommandReturn(struct Interp *i, int argc, char **argv, void *pd) {
	if (argc != 1 && argc != 2) return ArityErr(i, argv[0]);
	SetResult(i, (argc == 2) ? argv[1] : "");
	return _RETURN;
}
int CommandClear(struct Interp *i, int argc, char **argv, void *pd)
{
	system("cls");
	return 0;
}
int CommandStep(struct Interp *i, int argc, char **argv, void *pd)
{
	stepcounter += 1000;
	std::string acknowledgement;
	Storage inMsg;
	A.send_commandSimulationStep(stepcounter);
	A.check_resultState(inMsg, CMD_SIMSTEP2, false, &acknowledgement);

	return 0;
}
int CommandHelp(struct Interp *i, int argc, char **argv, void *pd)
{
	cout << "Help for this interpreter: " << endl
		<< "Available Instructions: " << endl
		<< "Set, Puts, If, While, Break, Continue, Proc, Return, Clear " << endl
		<< "Available math instructions: " << endl
		<< "Sum(+) Difference(-) Multiplication(*) Division(/) Modulo(%)" << endl
		<< "Example: '+ 10 24' will return sum of 10 and 24" << endl
		<< "To end type exit" << endl;
	return 0;
}

int CommandExit(struct Interp *i, int argc, char **argv, void *pd)
{
	A.send_commandClose();
	exit(1);
	return 0;
}
int CommandOpen(struct Interp *i, int argc, char **argv, void *pd)
{
	struct Interp interp;
	char buf[1024 * 16];
	FILE *fp = fopen(argv[1], "r");
	if (!fp) {
		perror("open");
	}
	buf[fread(buf, 1, 1024 * 16, fp)] = '\0';
	fclose(fp);
	Eval(i, buf);
	return 0;
}
int CommandSetVar(struct Interp *i, int argc, char **argv, void *pd)
{
	xml_document <> seqFile;
	string progvar, timevar, id, varj, numL, tfix, rob;
	ifstream sFile(argv[1]);
	if (!sFile.is_open()) {
		perror("open");
	}
	vector<char> bufferS((istreambuf_iterator<char>(sFile)), istreambuf_iterator<char>());
	bufferS.push_back('\0');
	seqFile.parse<0>(&bufferS[0]);
	sFile.close();
	ofstream outputFile(argv[3]);
	ifstream inputFile(argv[2]);
	int it = 0;
	int j = 0;
	xml_node<>* lightsNode = seqFile.first_node()->first_node()->first_node("junction");
	while (j < 30)
	{
		id = lightsNode->first_attribute("id")->value();
		numL = lightsNode->first_attribute("numL")->value();
		it = stoi(numL);
		tfix = lightsNode->first_attribute("time2")->value();
		timevar = "set tabtime" + id + " ";
		progvar = "set tabsek" + id + " ";
		varj = "set idS" + id + " " + id;
		timevar = timevar + lightsNode->first_attribute("time1")->value() + "5" + lightsNode->first_attribute("time1")->value() + "5";
		rob = lightsNode->first_attribute("valueL")->value();
		int z = it;
		int h = 0;
		for (z; z <= rob.size(); z = z + it+1)
		{
			if (h == 0 || h == 2) rob.insert(z, tfix);
			if (h == 1 || h == 3) rob.insert(z, "0");
			h++;
		}
		progvar = progvar + rob;
		lightsNode = lightsNode->next_sibling("junction");
		j++;
		outputFile << timevar << endl;
		outputFile << progvar << endl;
		outputFile << varj << endl;
	}
	cout << "done";
	outputFile << inputFile.rdbuf();

	inputFile.close();
	outputFile.close();


	return 0;
}
void RegisterCoreCommands(struct Interp *i) {
	int j; char *name[] = { "+", "-", "*", "/", "%", ">", ">=", "<", "<=", "==", "!=" };
	for (j = 0; j < (int)(sizeof(name) / sizeof(char*)); j++)
	RegisterCommand(i, name[j], CommandMath, NULL);
	RegisterCommand(i, "set", CommandSet, NULL);
	RegisterCommand(i, "step", CommandStep, NULL);
	RegisterCommand(i, "gettime", CommandGetTab, NULL);
	RegisterCommand(i, "getfix", CommandGetFix, NULL);
	RegisterCommand(i, "getoff", CommandGetTab3, NULL);
	RegisterCommand(i, "gettab", CommandGetTab2, NULL);
	RegisterCommand(i, "setvar", CommandSetVar, NULL);
	RegisterCommand(i, "settab", CommandSetTab, NULL);
	RegisterCommand(i, "get", CommandGet, NULL);
	RegisterCommand(i, "setlights", CommandSetLights, NULL);
	RegisterCommand(i, "puts", CommandPuts, NULL);
	RegisterCommand(i, "if", CommandIf, NULL);
	RegisterCommand(i, "while", CommandWhile, NULL);
	RegisterCommand(i, "break", CommandRetCodes, NULL);
	RegisterCommand(i, "continue", CommandRetCodes, NULL);
	RegisterCommand(i, "proc", CommandProc, NULL);
	RegisterCommand(i, "return", CommandReturn, NULL);
	RegisterCommand(i, "clear", CommandClear, NULL);
	RegisterCommand(i, "help", CommandHelp, NULL);
	RegisterCommand(i, "exit", CommandExit, NULL);
	RegisterCommand(i, "open", CommandOpen, NULL);
}

int main(int argc, char **argv) {
	struct Interp interp;
	InitInterp(&interp);
	RegisterCoreCommands(&interp);
	const char* d;
	cout << " SUMOConnector Start" << endl;
//	while (1){};
	d = "start legnicka.sumocfg";
	system(d);

	A.connect(host1, port1);
	if (argc == 1) {
		while (1) {
			char clibuf[1024];
			int retcode;
			printf("InterP> "); fflush(stdout);
			if (fgets(clibuf, 1024, stdin) == NULL) return 0;
			/*if (z == 0){
				z=1;
				string d = "setvar tab_seq.xml szkielet.txt program.txt";
				sprintf(clibuf, "%s", d.c_str());
			}
			if (z == 2)
			{
				z = 4;
				string d = "open program.txt";
				sprintf(clibuf, "%s", d.c_str());
			}*/
			retcode = Eval(&interp, clibuf);
			z++;
			if (interp.result[0] != '\0')
				printf("[%d] %s\n", retcode, interp.result);
		}
	}
	else if (argc == 2) {
		char buf[1024 * 16];
		FILE *fp = fopen(argv[1], "r");
		if (!fp) {
			perror("open"); //exit(1);
		}
		buf[fread(buf, 1, 1024 * 16, fp)] = '\0';
		fclose(fp);
		if (Eval(&interp, buf) != _OK) printf("%s\n", interp.result);
	}
	return 0;
}