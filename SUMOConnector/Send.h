#include "Storage.h"
#include "Socket.h"
#include "SUMOTime.h"
#include <iostream>
#include <map>
#define CMD_SIMSTEP2 0x02
#define CMD_CLOSE 0X7F
#define CMD_SET_TL_VARIABLE 0xc2
#define CMD_GET_INDUCTIONLOOP_VARIABLE 0xa0
#define CMD_STOP 0X12
#define blink 0x04
#define host1 "localhost"
#define port1 8873
#define SUMOReal double

//class Send{
//
//public:
//	Socket mySocket = Socket(host2, port2);
//	void send_commandSimulationStep(SUMOTime time);
//	void send_commandClose();
//	void connect(std::string host, int port);
//	void commandClose();
//	void send_commandSetValue(int domID, int varID, const std::string& objID, Storage& content);
//};
class TraCIAPI {
public:

	struct TraCIPosition {
		double x, y, z;
	};

	struct TraCIColor {
		int r, g, b, a;
	};

	typedef std::vector<TraCIPosition> TraCIPositionVector;

	struct TraCIBoundary {
		double xMin, yMin, zMin;
		double xMax, yMax, zMax;
	};
	class TraCIPhase {
	public:
		TraCIPhase(const SUMOTime _duration, const SUMOTime _duration1, const SUMOTime _duration2, const std::string& _phase)
			: duration(_duration), duration1(_duration1), duration2(_duration2), phase(_phase) {}
		~TraCIPhase() {}

		SUMOTime duration, duration1, duration2;
		std::string phase;
	};

	class TraCILogic {
	public:
		TraCILogic(const std::string& _subID, int _type, const std::map<std::string, SUMOReal>& _subParameter, unsigned int _currentPhaseIndex, const std::vector<TraCIPhase>& _phases)
			: subID(_subID), type(_type), subParameter(_subParameter), currentPhaseIndex(_currentPhaseIndex), phases(_phases) {}
		~TraCILogic() {}

		std::string subID;
		int type;
		std::map<std::string, SUMOReal> subParameter;
		unsigned int currentPhaseIndex;
		std::vector<TraCIPhase> phases;
	};

	class TraCILink {
	public:
		TraCILink(const std::string& _from, const std::string& _via, const std::string& _to)
			: from(_from), via(_via), to(_to) {}
		~TraCILink() {}

		std::string from;
		std::string via;
		std::string to;
	};
	class TraCIScopeWrapper {
	public:
		TraCIScopeWrapper(TraCIAPI& parent) : myParent(parent) {}

		virtual ~TraCIScopeWrapper() {}


	protected:
		TraCIAPI& myParent;


	private:
		TraCIScopeWrapper(const TraCIScopeWrapper& src);

		TraCIScopeWrapper& operator=(const TraCIScopeWrapper& src);

	};

	class EdgeScope : public TraCIScopeWrapper {
	public:
		EdgeScope(TraCIAPI& parent) : TraCIScopeWrapper(parent) {}
		virtual ~EdgeScope() {}

		std::vector<std::string> getIDList() const;
		unsigned int getIDCount() const;
		SUMOReal getAdaptedTraveltime(const std::string& edgeID, SUMOTime time) const;
		SUMOReal getEffort(const std::string& edgeID, SUMOTime time) const;
		SUMOReal getCO2Emission(const std::string& edgeID) const;
		SUMOReal getCOEmission(const std::string& edgeID) const;
		SUMOReal getHCEmission(const std::string& edgeID) const;
		SUMOReal getPMxEmission(const std::string& edgeID) const;
		SUMOReal getNOxEmission(const std::string& edgeID) const;
		SUMOReal getFuelConsumption(const std::string& edgeID) const;
		SUMOReal getNoiseEmission(const std::string& edgeID) const;
		SUMOReal getLastStepMeanSpeed(const std::string& edgeID) const;
		SUMOReal getLastStepOccupancy(const std::string& edgeID) const;
		SUMOReal getLastStepLength(const std::string& edgeID) const;
		SUMOReal getTraveltime(const std::string& edgeID) const;
		unsigned int getLastStepVehicleNumber(const std::string& edgeID) const;
		SUMOReal getLastStepHaltingNumber(const std::string& edgeID) const;
		std::vector<std::string> getLastStepVehicleIDs(const std::string& edgeID) const;

		void adaptTraveltime(const std::string& edgeID, SUMOReal time) const;
		void setEffort(const std::string& edgeID, SUMOReal effort) const;
		void setMaxSpeed(const std::string& edgeID, SUMOReal speed) const;

	private:
		EdgeScope(const EdgeScope& src);

		EdgeScope& operator=(const EdgeScope& src);

	};

	class InductionLoopScope : public TraCIScopeWrapper {
	public:
		InductionLoopScope(TraCIAPI& parent) : TraCIScopeWrapper(parent) {}
		virtual ~InductionLoopScope() {}

		std::vector<std::string> getIDList() const;
		SUMOReal  getPosition(const std::string& loopID) const;
		std::string getLaneID(const std::string& loopID) const;
		unsigned int getLastStepVehicleNumber(const std::string& loopID) const;
		SUMOReal getLastStepMeanSpeed(const std::string& loopID) const;
		std::vector<std::string> getLastStepVehicleIDs(const std::string& loopID) const;
		SUMOReal getLastStepOccupancy(const std::string& loopID) const;
		SUMOReal getLastStepMeanLength(const std::string& loopID) const;
		SUMOReal getTimeSinceDetection(const std::string& loopID) const;
		unsigned int getVehicleData(const std::string& loopID) const;

	private:
		InductionLoopScope(const InductionLoopScope& src);

		InductionLoopScope& operator=(const InductionLoopScope& src);

	};
	class TrafficLightScope : public TraCIScopeWrapper {
	public:
		TrafficLightScope(TraCIAPI& parent) : TraCIScopeWrapper(parent) {}
		virtual ~TrafficLightScope() {}

		std::vector<std::string> getIDList() const;
		std::string getRedYellowGreenState(const std::string& tlsID) const;
		std::vector<TraCIAPI::TraCILogic> getCompleteRedYellowGreenDefinition(const std::string& tlsID) const;
		std::vector<std::string> getControlledLanes(const std::string& tlsID) const;
		std::vector<TraCIAPI::TraCILink> getControlledLinks(const std::string& tlsID) const;
		std::string getProgram(const std::string& tlsID) const;
		unsigned int getPhase(const std::string& tlsID) const;
		unsigned int getNextSwitch(const std::string& tlsID) const;

		void setRedYellowGreenState(const std::string& tlsID, const std::string& state) const;
		void setPhase(const std::string& tlsID, unsigned int index) const;
		void setProgram(const std::string& tlsID, const std::string& programID) const;
		void setPhaseDuration(const std::string& tlsID, unsigned int phaseDuration) const;
		void setCompleteRedYellowGreenDefinition(const std::string& tlsID, const TraCIAPI::TraCILogic& logic) const;

	private:
		TrafficLightScope(const TrafficLightScope& src);

		TrafficLightScope& operator=(const TrafficLightScope& src);

	};
	TraCIAPI();
	~TraCIAPI();
	std::vector<std::string> getStringVector(int cmd, int var, const std::string& id,Storage* add = 0);
	/*Socket mySocket = Socket(host1, port1);*/
	void send_commandSimulationStep(SUMOTime time);
	void send_commandClose();
	int getIntVeh(int cmd, int var, const std::string& id, Storage* add=0);
	int getInt(int cmd, int var, const std::string& id, Storage* add = 0);
	void connect(std::string host, int port);
	void commandClose();
	void send_commandSetValue(int domID, int varID, const std::string& objID, Storage& content);
	void check_resultState(Storage& inMsg, int command, bool ignoreCommandId = false, std::string* acknowledgement = 0)const;
	TrafficLightScope trafficlights;
	InductionLoopScope loop;
	EdgeScope edge;
protected:
	void send_commandGetVariable(int domID, int varID, const std::string& objID, Storage* add = 0);
	
	void check_commandGetResult(Storage& inMsg, int command, int expectedType = -1, bool ignoreCommandId = false) const;
	void processGET(Storage& inMsg, int command, int expectedType, bool ignoreCommandId = false) const;
	Socket* mySocket;
};
	

	