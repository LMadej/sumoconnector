#include "Storage.h"
#include <iostream>
#include <iterator>
#include <sstream>
#include <cassert>
#include <algorithm>
#include <iomanip>
#include <string>
std::ofstream file;
Storage::Storage()
{
	init();
	
}
Storage::Storage(const unsigned char packet[], int length)
{
	// Length is calculated, if -1, or given
	if (length == -1) length = sizeof(packet) / sizeof(unsigned char);

	store.reserve(length);
	// Get the content
	for (int i = 0; i < length; ++i) store.push_back(packet[i]);

	init();
}
bool Storage::valid_pos()
{
	return (iter_ != store.end());
}

void Storage::init()
{
	// Initialize local variables
	iter_ = store.begin();

	short a = 0x0102;
	unsigned char *p_a = reinterpret_cast<unsigned char*>(&a);
	bigEndian_ = (p_a[0] == 0x01); // big endian?
}
void Storage::writeInt(int value) throw()
{
	unsigned char *p_value = reinterpret_cast<unsigned char*>(&value);
	writeByEndianess(p_value, 4);
}
void Storage::writeByEndianess(const unsigned char * begin, unsigned int size)
{
	const unsigned char * end = &(begin[size]);
	if (bigEndian_)
		store.insert(store.end(), begin, end);
	else
		store.insert(store.end(), std::reverse_iterator<const unsigned char *>(end), std::reverse_iterator<const unsigned char *>(begin));
	iter_ = store.begin();
}
int Storage::readUnsignedByte() throw(std::invalid_argument)
{
	return static_cast<int>(readChar());
}
void Storage::writeUnsignedByte(int value) throw(std::invalid_argument)
{
	if (value < 0 || value > 255)
	{
		throw std::invalid_argument("Storage::writeUnsignedByte(): Invalid value, not in [0, 255]");
	}
	writeChar(static_cast<unsigned char>(value));
}
int Storage::readInt() throw(std::invalid_argument)
{
	int value = 0;
	unsigned char *p_value = reinterpret_cast<unsigned char*>(&value);
	readByEndianess(p_value, 4);
	return value;
}
void Storage::reset()
{
	store.clear();
	iter_ = store.begin();
}
unsigned char Storage::readChar() 
{
	if (!valid_pos())
	{
		std::cerr<<"Storage::readChar(): invalid position";
	}
	return readCharUnsafe();
}
void Storage::writeChar(unsigned char value) 
{
	store.push_back(value);
	iter_ = store.begin();
}
unsigned char Storage::readCharUnsafe()
{
	char hb = *iter_;
	++iter_;
	return hb;
}
void Storage::readByEndianess(unsigned char * array, int size)
{
	checkReadSafe(size);
	if (bigEndian_)
	{
		for (int i = 0; i < size; ++i)
			array[i] = readCharUnsafe();
	}
	else
	{
		for (int i = size - 1; i >= 0; --i)
			array[i] = readCharUnsafe();
	}
}
void Storage::checkReadSafe(unsigned int num) const  
{
	file.open("log.txt");
	file << "Log file" << std::endl;
	file<< "Storage::checkreadsafe"<<std::endl;
	file << "Store size: "<< store.size()<<std::endl;
	file << "Dist: " << std::distance(iter_, store.end()) << std::endl;
	file.close();
	if (std::distance(iter_, store.end())< static_cast<int>(num))
	{
		std::cerr << "Storage::readIsSafe: want to read " << num << " bytes from Storage, "
			<< "but only " << std::distance(iter_, store.end()) << " remaining";
	}
}
void Storage::writeString(const std::string &s) throw()
{
	writeInt(static_cast<int>(s.length()));

	store.insert(store.end(), s.begin(), s.end());
	iter_ = store.begin();
}
unsigned int Storage::position() const
{
	return static_cast<unsigned int>(std::distance(store.begin(), iter_));
}

void Storage::writeStorage(Storage& other)
{
	store.insert<StorageType::const_iterator>(store.end(), other.iter_, other.store.end());
	iter_ = store.begin();
}
std::string Storage::readString() throw(std::invalid_argument)
{
	int len = readInt();
	checkReadSafe(len);
	StorageType::const_iterator end = iter_;
	std::advance(end, len);
	const std::string tmp(iter_, end);
	iter_ = end;
	return tmp;
}
void Storage::writePacket(unsigned char* packet, int length)
{
	store.insert(store.end(), &(packet[0]), &(packet[length]));
	iter_ = store.begin();  
}

void Storage::writePacket(const std::vector<unsigned char> &packet)
{
	std::copy(packet.begin(), packet.end(), std::back_inserter(store));
	iter_ = store.begin();
}
