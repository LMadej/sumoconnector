#include "Socket.h"
#include <sys/types.h>
#include <WinSock2.h>
//#include <netinet/in.h>
//#include <netinet/tcp.h>
//#include <arpa/inet.h>
//#include <netdb.h>
#include <errno.h>
#include <fcntl.h>
#include <iostream>
#include <cassert>
//#include <unistd.h>
#pragma comment(lib, "Ws2_32.lib")

#ifdef WIN32
bool Socket::init_windows_sockets_ = true;
bool Socket::windows_sockets_initialized_ = false;
int Socket::instance_count_ = 0;
#endif

const int Socket::lengthLen = 4;

Socket::Socket(std::string host, int port)
: host_(host),
port_(port),
socket_(-1),
server_socket_(-1),
blocking_(true),
verbose_(false)
{
	init();
}

void Socket::init()
{
#ifdef WIN32
	instance_count_++;

	if (init_windows_sockets_ && !windows_sockets_initialized_)
	{
		WSAData wsaData;
		if (WSAStartup(MAKEWORD(1, 1), &wsaData) != 0)
			BailOnSocketError("Unable to init WSA Sockets");
		windows_sockets_initialized_ = true;
	}

#endif
}
size_t Socket::recvAndCheck(unsigned char * const buffer, std::size_t len)
const
{
#ifdef WIN32
	const int bytesReceived = recv(socket_, (char*)buffer, static_cast<int>(len), 0);
#else
	const int bytesReceived = static_cast<int>(recv(socket_, buffer, len, 0));
#endif
	if (bytesReceived == 0)
		std::cerr << "tcpip::Socket::recvAndCheck @ recv: peer shutdown";
	if (bytesReceived < 0)
		std::cout << "tcpip::Socket::recvAndCheck @ recv";

	return static_cast<size_t>(bytesReceived);
}

void Socket::receiveComplete(unsigned char * buffer, size_t len) const
{
	while (len > 0)
	{
		const size_t bytesReceived = recvAndCheck(buffer, len);
		len -= bytesReceived;
		buffer += bytesReceived;
	}
}
bool Socket::receiveExact(Storage &msg)

{
	// buffer for received bytes
	// According to the C++ standard elements of a std::vector are stored
	// contiguously. Explicitly &buffer[n] == &buffer[0] + n for 0 <= n < buffer.size().
	std::vector<unsigned char> buffer(lengthLen);

	// receive length of TraCI message
	receiveComplete(&buffer[0], lengthLen);
	Storage length_storage(&buffer[0], lengthLen);
	const int totalLen = length_storage.readInt();
	assert(totalLen > lengthLen);

	// extent buffer
	buffer.resize(totalLen);

	// receive remaining TraCI message
	receiveComplete(&buffer[lengthLen], totalLen - lengthLen);

	// copy message content into passed Storage
	msg.reset();
	msg.writePacket(&buffer[lengthLen], totalLen - lengthLen);

	//printBufferOnVerbose(buffer, "Rcvd Storage with");

	return true;
}
bool Socket::atoaddr(std::string address, struct in_addr& addr)
{
	struct hostent* host;
	struct in_addr saddr;

	// First try nnn.nnn.nnn.nnn form
	saddr.s_addr = inet_addr(address.c_str());
	if (saddr.s_addr != static_cast<unsigned int>(-1))
	{
		addr = saddr;
		return true;
	}

	host = gethostbyname(address.c_str());
	if (host) {
		addr = *((struct in_addr*)host->h_addr_list[0]);
		return true;
	}

	return false;
}

#ifdef WIN32
// ----------------------------------------------------------------------
std::string
Socket::
GetWinsockErrorString(int err)
const
{

	switch (err)
	{
	case 0:                 return "No error";
	case WSAEINTR:          return "Interrupted system call";
	case WSAEBADF:          return "Bad file number";
	case WSAEACCES:         return "Permission denied";
	case WSAEFAULT:         return "Bad address";
	case WSAEINVAL:         return "Invalid argument";
	case WSAEMFILE:         return "Too many open sockets";
	case WSAEWOULDBLOCK:    return "Operation would block";
	case WSAEINPROGRESS:    return "Operation now in progress";
	case WSAEALREADY:       return "Operation already in progress";
	case WSAENOTSOCK:       return "Socket operation on non-socket";
	case WSAEDESTADDRREQ:   return "Destination address required";
	case WSAEMSGSIZE:       return "Message too long";
	case WSAEPROTOTYPE:     return "Protocol wrong type for socket";
	case WSAENOPROTOOPT:    return "Bad protocol option";
	case WSAEPROTONOSUPPORT:    return "Protocol not supported";
	case WSAESOCKTNOSUPPORT:    return "Socket type not supported";
	case WSAEOPNOTSUPP:     return "Operation not supported on socket";
	case WSAEPFNOSUPPORT:   return "Protocol family not supported";
	case WSAEAFNOSUPPORT:   return "Address family not supported";
	case WSAEADDRINUSE:     return "Address already in use";
	case WSAEADDRNOTAVAIL:  return "Can't assign requested address";
	case WSAENETDOWN:       return "Network is down";
	case WSAENETUNREACH:    return "Network is unreachable";
	case WSAENETRESET:      return "Net Socket reset";
	case WSAECONNABORTED:   return "Software caused tcpip::Socket abort";
	case WSAECONNRESET:     return "Socket reset by peer";
	case WSAENOBUFS:        return "No buffer space available";
	case WSAEISCONN:        return "Socket is already connected";
	case WSAENOTCONN:       return "Socket is not connected";
	case WSAESHUTDOWN:      return "Can't send after socket shutdown";
	case WSAETOOMANYREFS:   return "Too many references, can't splice";
	case WSAETIMEDOUT:      return "Socket timed out";
	case WSAECONNREFUSED:   return "Socket refused";
	case WSAELOOP:          return "Too many levels of symbolic links";
	case WSAENAMETOOLONG:   return "File name too long";
	case WSAEHOSTDOWN:      return "Host is down";
	case WSAEHOSTUNREACH:   return "No route to host";
	case WSAENOTEMPTY:      return "Directory not empty";
	case WSAEPROCLIM:       return "Too many processes";
	case WSAEUSERS:         return "Too many users";
	case WSAEDQUOT:         return "Disc quota exceeded";
	case WSAESTALE:         return "Stale NFS file handle";
	case WSAEREMOTE:        return "Too many levels of remote in path";
	case WSASYSNOTREADY:    return "Network system is unavailable";
	case WSAVERNOTSUPPORTED:    return "Winsock version out of range";
	case WSANOTINITIALISED: return "WSAStartup not yet called";
	case WSAEDISCON:        return "Graceful shutdown in progress";
	case WSAHOST_NOT_FOUND: return "Host not found";
	case WSANO_DATA:        return "No host data of that type was found";
	}

	return "unknown";
}

#endif // WIN32

void Socket::BailOnSocketError(std::string context)
{
#ifdef WIN32
	int e = WSAGetLastError();
	std::string msg = GetWinsockErrorString(e);
#else
	std::string msg = strerror(errno);
#endif
}
void Socket::connect()
{
	in_addr addr;
	if (!atoaddr(host_.c_str(), addr))
		BailOnSocketError("tcpip::Socket::connect() @ Invalid network address");

	sockaddr_in address;
	memset((char*)&address, 0, sizeof(address));
	address.sin_family = AF_INET;
	address.sin_port = htons(port_);
	address.sin_addr.s_addr = addr.s_addr;

	socket_ = static_cast<int>(socket(PF_INET, SOCK_STREAM, 0));
	if (socket_ < 0)
		BailOnSocketError("tcpip::Socket::connect() @ socket");

	if (::connect(socket_, (sockaddr const*)&address, sizeof(address)) < 0)
		BailOnSocketError("tcpip::Socket::connect() @ connect");

	if (socket_ >= 0)
	{
		int x = 1;
		setsockopt(socket_, IPPROTO_TCP, TCP_NODELAY, (const char*)&x, sizeof(x));
	}
}
void Socket::send(const std::vector<unsigned char> &buffer)
{
	if (socket_ < 0)
		return;

	size_t numbytes = buffer.size();
	unsigned char const *bufPtr = &buffer[0];
	while (numbytes > 0)
	{
#ifdef WIN32
		int bytesSent = ::send(socket_, (const char*)bufPtr, static_cast<int>(numbytes), 0);
#else
		int bytesSent = ::send(socket_, bufPtr, numbytes, 0);
#endif
		if (bytesSent < 0)
			BailOnSocketError("send failed");

		numbytes -= bytesSent;
		bufPtr += bytesSent;
	}
}
void Socket::sendExact(const Storage &b)
{
	int length = static_cast<int>(b.size());
	Storage length_storage;
	length_storage.writeInt(lengthLen + length);

	// Sending length_storage and b independently would probably be possible and
	// avoid some copying here, but both parts would have to go through the
	// TCP/IP stack on their own which probably would cost more performance.
	std::vector<unsigned char> msg;
	msg.insert(msg.end(), length_storage.begin(), length_storage.end());
	msg.insert(msg.end(), b.begin(), b.end());
	send(msg);
}

void Socket::close()
{
	// Close client-connection 
	if (socket_ >= 0)
	{
#ifdef WIN32
		///		::closesocket(socket_);
#else
		::close(socket_);
#endif

		socket_ = -1;
	}
}